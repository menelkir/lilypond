Lilypond Repository
===================

Several music charts made using lilypond.

Repositories
============

* `Ultima Music <https://127.0.0.1>`_
* `Music typeset with the Lilypond system <https://github.com/cellist/Lilypond-Sheet-Music.git>`_
* `Bagpipe Music <https://github.com/svenax/bagpipemusic.git>`_
* `Beautiful LilyPond scores under free licenses <https://github.com/wbsoft/lilymusic>`_