Ultima Sheet Music
==================

Tired of replaying your Final Fantasy Piano Collection for the billionth time? Rejoyce! I have typeset some memorable tunes from the Ultima game series for your playing pleasure. All of the sheet music below was transcribed from MIDI files found on the Internet using GNU Lilypond and is arranged for piano and/or organ (2 manuals and bass pedals). I've left out tempo specifications and used dynamic and articulation marks very sparingly, so you should refer to the original tunes when in doubt. Enjoy!

All music is Copyright © Origin Systems, Inc.

Original Website: https://www.cebix.net/misc.html

==========
Downloads:
==========

`All tunes in PDF format (480k) <https://www.cebix.net/downloads/ultima-music.zip>`_

`Lilypond source files for all tunes (10k) <https://www.cebix.net/downloads/ultima-music-src.tar.gz>`_

===============
Tunes included:
===============

**Ultima IV - Quest Of The Avatar**

* `Fanfare Of Lord British <fanfare.ly>`_
* `Combat Theme <u4combat.ly>`_

**Ultima V - Warriors Of Destiny**

* `Dream Of Lady Nan <dream.ly>`_
* `Fanfare For The Virtues <virtues.ly>`_
* `Greyson's Tale (arranged for organ, but piano players can just omit the bass voice) <greyson.ly>`_
* `Lord Blackthorn <blackthorn.ly>`_
* `Main Theme <u5main.ly>`_
* `Stones <stones.ly>`_
* `Villager Tarantella <tarantella.ly>`_

**Ultima VI - The False Prophet**

* `Audchar Gargl Zenmúr (original arrangement) <audchar.ly>`_
* `Audchar Gargl Zenmúr (an arrangement that is easier to play) <audchar2.ly>`_
* `Dungeon Theme <dungeon.ly>`_
* `Forest Theme <forest.ly>`_

**Ultima VII - The Black Gate**

* `Camping Theme (unless you have big hands, leave out the upper voice in bars 25-29 :-) <camping.ly>`_
* `Fellowship Theme <fellowship.ly>`_
* `Love Theme <love.ly>`_
* `Wilderness <wilderness.ly>`_
* `The Wisps <wisps.ly>`_

**Ultima VII Part 2 - Serpent Isle**

* `The Dark Path (this sounds crappy on the piano, but I wanted to include a tune from Serpent Isle…) <darkpath.ly>`_
