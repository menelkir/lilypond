\version "1.3.129";

\header {
	title = "Combat Theme";
	subtitle = "From Ultima IV --- Quest Of The Avatar";
	composer = "Kenneth W. Arnold";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1985 Origin Systems, Inc.";
}

global = \notes {
	\key g\minor;
	\time 3/4;
}

melody = \notes \relative c''' \context Voice = melody {
	g4. \times 2/3 {d16 e f} e8 c |
	d8. bes16 g2 |
	g'4. f16 g a8 f |
	d2. |

	g4. \times 2/3 {d16 e f} e8 c |
	d8. bes16 g4. g16 a |
	< {\stemUp bes8 c16 d c8 bes c a} \context Voice = melodyTwo {\stemDown g8. g16 g4 f} > | \stemBoth
	g4 g2 |

	\bar ":|";
}

bassOne = \notes \relative c' \context Voice = bassOne { \stemUp
	d8. d16 d4 e |
	d8. d16 d2 |
	d8. d16 d4 f |
	f8. f16 f4 fis |

	g8. d16 d4 e |
	d8. d16 d2 |
	\skip 2.; |
	d8. d16 d2 |

	\bar ":|";
}

bassTwo = \notes \relative c' \context Voice = bassTwo { \stemDown
	g4. g8 c c |
	g4. d8 g d |
	g4. g8 c c |
	bes4. bes8 d d |

	g,4. g8 c c |
	g2 f4 |
	es2 f4 |
	g4. g16 a bes a g a_"F.Q." |

	\bar ":|";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			<\bassOne \bassTwo>
		}
	>

%	\midi { \tempo 4 = 120; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
