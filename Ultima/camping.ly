\version "1.3.129";

\header {
	title = "Camping Theme";
	subtitle = "From Ultima VII --- The Black Gate";
	composer = "D.~Glover";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1992 Origin Systems, Inc.";
}

global = \notes {
	\key c\major;
	\time 3/4;
}

melody = \notes \relative c' \context Voice = melody {
	\grace {[c16 d]} e4 f a | g2 e4 |
	d e f | e2 d4 |
	c d g | e2 c4 |
	b c d | c2 b4 |
	\break

	a b c | g c b |
	a b c | g c b |
	a b c | f e d |
	c2. | c2. |
	\break

	e4 f g | e <{\stemDown a e} \context Voice = melodyTwo {\stemUp c'8 b a g}> | \stemUp
	d4 e f | d <{\stemDown g d} \context Voice = melodyTwo {\stemUp b'8 a g f}> | \stemUp
	c4 d e | c g' c, |
	b4 c d | b e b |
	\break

	<{\stemDown a b c | b c d | g, c b | a2. | a4 b c} \context Voice = melodyTwo {\stemUp a'2. | g2 ~ g8 a | b2 ~ b8 c | a4 c g | f2.}> | \stemUp
	f4 e4. c8 |
	d2 d8 e16 d | c2. |

	\bar "|.";
}

bassOne = \notes \relative c' \context Voice = bassOne { \stemUp
	r4 c2 | r4 c2 |
	r4 b2 | r4 b2 |
	r4 a2 | r4 a2 |
	r4 g2 | r4 g2 |

	r4 f2 | r4 e2 |
	r4 f2 | r4 e2 |
	r4 f2 | r4 g2 |
	c,2. | c, |

	c'8( g' )c2 | c,8( g' )c2 |
	b,8( fis' )b2 | b,8( fis' )b2 |
	a,8( e' )a2 | a,8( e' )a2 |
	g,8( d' )g2 | g,8( d' )g2 |

	f,8( c' )f2 | g,8( d' )g2 |
	e,8( b' )e2 | f,8( c' )f2 |
	f,8( c' )f2 | g,8( d' )g2 |
	f2. | \skip 2.; |

	\bar "|.";
}

bassTwo = \notes \relative c \context Voice = bassTwo { \stemDown
	c2. | c |
	b | b |
	a | a |
	g | g |

	f2 ~ f8 f | e2 ~ e8 e |
	f2. | e |
	f | g |
	\skip 2.; | \skip 2.; |

	\repeat unfold 14 { \skip 2.; }
	f2 f4 | c'2. |
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			<\bassOne \bassTwo>
		}
	>

%	\midi { \tempo 4 = 110; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
