\version "1.3.129";

\header {
	title = "Dream Of Lady Nan";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Markus Brenner";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key c\major;
	\time 4/4;
}

melody = \notes \relative c'' \context Voice = melody {
	<f8 a> <e16 g> <f a> <d8 f> <c16 e> <d f> <a4 d> <a c> |
	<g8. b> <a16 c> <b2. d> |
	<f'8 a> <e16 g> <f a> <d8 f> <c16 e> <d f> <a4 d> <a c> |
	<\stemDown g4 \context Voice = melodyTwo {\stemUp b8. d16}> \stemBoth g2. |
	\break

	<e'8 g> <d16 f> <e g> <c4 \context Voice = melodyTwo {\stemUp e8 d16 e}> \stemBoth <g4 c> <g bes> |
	<f8. a> <g16 bes> <a2. c> |
	<e'8 g> <d16 f> <e g> <c4 \context Voice = melodyTwo {\stemUp e8 d16 e}> \stemBoth <g4 c> <g bes> |
	<\stemDown {f4 f e e} \context Voice = melodyTwo {\stemUp a8. c16 \skip 4; a8. cis16 \skip 4;}> | \stemBoth
	\break

	\property Voice.tupletDirection = \up
	<f'8 a> \times 2/3 {[<e16 g> <f a> <e g>]} <d8 f> \times 2/3 {[<c16 e> <d f> <c e>]} <a4 d> <a c> |
	<g8. b> <a16 c> <b2. d> |
	<f'8 a> \times 2/3 {[<e16 g> <f a> <e g>]} <d8 f> \times 2/3 {[<c16 e> <d f> <c e>]} <a4 d> <a c> |
	<\stemDown g4 \context Voice = melodyTwo {\stemUp b8. d16}> \stemBoth g2. |
	\break

	<e'8 g> \times 2/3 {[<d16 f> <e g> <d f>]} <c4 \context Voice = melodyTwo {\stemUp e8 \times 2/3 {[d16 e d]}}> \stemBoth <g4 c> <g bes> |
	<f8. a> <g16 bes> <a2. c> |
	<e'8 g> \times 2/3 {[<d16 f> <e g> <d f>]} <c4 \context Voice = melodyTwo {\stemUp e8 \times 2/3 {[d16 e d]}}> \stemBoth <g4 c> <g bes> |
	<\stemDown {f4 f e e} \context Voice = melodyTwo {\stemUp a8. c16 \skip 4; a8. cis16 \skip 4;}> | \stemBoth
	\break

	<
		{ \stemDown
			fis4 d8 fis g4. f8 |
			e4 c8 e f2 |
			es2 bes4. bes8 |
			e2 fis |
		}
		\context Voice = melodyTwo { \stemUp
			d'4 a8 c c4 b |
			c g16 a bes8 bes4 a |
			bes4 a8 g d4. d8 |
			bes'4 a8 g a2 |
		}
	> \stemBoth
	\break

	<bes8 d> <a16 c> <bes d> <g8 bes> <f16 a> <g bes> <d4 g> <d f> |
	<c8. e> <d16 f> <e2. g> |
	<bes'8 d> <a16 c> <bes d> <g8 bes> <f16 a> <g bes> <d4 g> <d f> |
	<\stemDown c4 \context Voice = melodyTwo {\stemUp e8. g16}> \stemBoth c2. |
	\break

	<a'8 c> <g16 bes> <a c> <f4 \context Voice = melodyTwo {\stemUp a8 g16 a}> \stemBoth <c4 f> <c es> |
	<bes8. d> <c16 es> <d4 f> r8 <bes d> <c es> <d f> |
	<c8. e> <d16 f> <e4 g> r8 <c e> <d f> <e g> |
	<\context Voice = melodyTwo {\stemDown d4 e8 d} \stemUp a'2> \stemBoth <cis2 a'> |

	\bar ":|";
}

bass = \notes \relative c \context Voice = bass {
	\repeat unfold 2 {
		d8 a' d a d, a' d d, |
		g, d' g d g, d' g g, |
	}

	c g' c g c, g' c c, |
	f, c' f c f, c' f f, |
	c' g' c g c, g' c c, |
	f, c' f c a e' a a, |

	\repeat unfold 2 {
		d8 a' d a d, a' d d, |
		g, d' g d g, d' g g, |
	}

	c g' c g c, g' c c, |
	f, c' f c f, c' f f, |
	c' g' c g c, g' c c, |
	f, c' f c a e' a a, |

	d, a' d fis g, d' g b |
	c,, g' c e f, c' f a |
	es, bes' es g g, d' g bes |
	c,, g' c e d, a' d fis |

	\repeat unfold 2 {
		g,8 d' g d g, d' g g, |
		c, g' c g c, g' c c, |
	}

	f c' f c f, c' f f, |
	bes, f' bes f bes,2 |
	c8 g' c g c,2 |
	a8 e' a e g e cis a_"F.Q." |

	\bar ":|";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 80; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
