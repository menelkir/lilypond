\version "1.3.129";

\header {
	title = "Fellowship Theme";
	subtitle = "From Ultima VII --- The Black Gate";
	composer = "D.~Glover";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1992 Origin Systems, Inc.";
}

global = \notes {
	\key d\minor;
	\time 6/8;
}

melody = \notes \relative c'' \context Voice = melody {
	a4. g4 a16 g |
	f4. f8 g f |
	< \context Voice = melodyTwo {
		e4. c ~ | \stemDown \tieDown
		c4. c8 d e |
		d2. ~ |
		d |
		c ~ |
		c |
		d2. ~ |
		d | \stemUp
		e4. c ~ | \stemDown
		c4. c8 d e |
		d2. |
	} \context Voice = melodyOne { \stemUp
		\skip 2.; |
		a'8 g g g f e |
		a4. g4 a16 g |
		f4. f8 g f |
		g2. | \stemBoth
		a8 g g g f e | \stemUp
		a4. g4 a16 g |
		f4. f4 g16 f |
		\skip 2.; |
		a8 g g g f e |
		a4. a4 g16 f |
	} > \stemBoth
	e4. e8 f g |
	f4. f8 g a |
	f2. |
	\break

	\key a\minor;
	\bar "||";

	\repeat unfold 3 {
		e4. c |
		d a |
	}
	c8 a c c a c |
	d b d d b d |
	\break

	c'4. a8 b g |
	< { \stemDown \tieDown
		r8 a,8 b c4 c16 b |
		a2. |
		b |
		a'4. r4. |
		r8 a,8 b c4. |
		c8 b a c b a |
		d c b d c b |
	} \context Voice = melodyTwo { \stemUp
		a'4 e2 |
		r8 e d c4 d16 c |
		r8 a'8 b c4 c16 b |
		c4. a8 b g |
		a4 e2 |
		fis2 e8 fis |
		g2. |
	} > \stemBoth

	\bar ":|";
}

accomp = \notes \relative c \context Voice = accomp {
	<f2. a d> | <f bes d> |
	<e g c> ~ | <e g c> |
	<f a d> | <f bes d> |
	<f4. g c> ~ <e g c> ~ | <e2. g c> |

	<f a d> | <f bes d> |
	<e g c> ~ | <e g c> |
	<f a d> | <e a c> |
	<f bes d> | <f a c> |

	\key a\minor;
	\bar "||";

	\repeat unfold 2 {
		<e a c> | <c f a> |
		<e a c> | <e g b> |
		<e a c> | <c f a> |
		<c fis a> | <d g b> |
	}

	\bar ":|";
}

bass = \notes \relative c \context Voice = bass {
	\repeat unfold 3 {
		d2. | bes | c ~ | c |
	}
	d | a | bes | f |

	\key a\minor;
	\bar "||";

	a | f | a | e |
	a | f | fis | g |
	a | f | a | e |
	a | f | fis | g_"F.Q." |

	\bar ":|";
}

names = \chords {
	d2.:min bes c r d2.:min bes \notes{<c4. f g>} c r2.
	d2.:min bes c r d2.:min a:min bes f
	a:min f a:min e:min a:min f fis:dim g
	a:min f a:min e:min a:min f fis:dim g
}

\score {
	\context Score <
		\context PianoStaff <
			\context Staff = top {
				\global
				\melody
			}
			\context Staff = bottom { \clef bass;
				\global
				\accomp
			}
		>
		\context ChordNames \names
		\context Staff = bass { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 100; }

	\paper {
		indent = 0;
		interscoreline = 12;
		\include "a4.ly"
	}
}
