\version "1.3.129";

\header {
	title = "Fanfare For The Virtues";
	subtitle = "From Ultima V --- Warriors of Destiny";
	composer = "Kenneth W. Arnold";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key c\major;
	\time 4/4;
}

melody = \notes \relative c' \context Voice = melody {
	<cis8 a'> <cis16 a'> <b e> <a8 cis> <b e> <cis4 a'> <e4 b'> |
	<e8 c'> <e16 c'> <d g> <c8 e> <d g> <e4 c'> <g4 d'> |
	<g8 es'> <g16 es'> <f bes> <es8 g> <f bes> <g4 es'> <bes4 f'> |
	<b2. g'> r4 |
	\break

	<c8 e> <c16 e> <g d'> <e8 c'> <g d'> <c4 e> <a f'> |
	<c8 e> <c16 e> <g d'> <e8 c'> <c' e> <g4 d'> <g d'> |
	<c8 e> <c16 e> <g d'> <e8 c'> <g d'> <c4 e> <a f'> |
	<c16 e> <g d'> <e c'> <c' e> <g8 d'> <g d'> <e2 c'> |
	\break

	<c'8 e> <c16 e> <g d'> <e8 c'> <g d'> <c4 e> <a f'> |
	<c8 e> <c16 e> <g d'> <e8 c'> <c' e> <g4 d'> <g d'> |
	<c8 e> <c16 e> <g d'> <e8 c'> <g d'> <c4 e> <a f'> |
	<c16 e> <g d'> <c e> <e, c'> <g8 d'> <g d'> <e2 c'> |
	\break

	\repeat volta 2 {
		<f8 a> <f16 a> <g b> <a8 c> <b d> <g4 b> <e g> |
		<f8 a> <f16 a> <g b> <a8 c> <b d> <c4 e> <c e> |
		<d8 f> <d16 f> <a e'> <f8 d'> <d' f> <a4 e'> <e c'> |
	} \alternative {
		{<g8 b> <g16 b> <d a'> <b8 g'> <g' b> <d4 a'> <c a'>}
		{<bes'8 d> <bes16 d> <f c'> <d8 bes'> <bes' d> <cis2 e>}
	}

	\key d\major;
	\bar "||";

	<d'8 fis> <d16 fis> <a e'> <fis8 d'> <a e'> <d4 fis> <b g'> |
	<d8 fis> <d16 fis> <a e'> <fis8 d'> <d' fis> <a4 e'> <a e'> |
	<d8 fis> <d16 fis> <a e'> <fis8 d'> <a e'> <d4 fis> <b g'> |
	<d16 fis> <a e'> <fis d'> <d' fis> <a8 e'> <a e'> <fis2 d'> |

	<d'8 fis> <d16 fis> <a e'> <fis8 d'> <a e'> <d4 fis> <b g'> |
	<d8 fis> <d16 fis> <a e'> <fis8 d'> <d' fis> <a4 e'> <a e'> |
	<d8 fis> <d16 fis> <a e'> <fis8 d'> <a e'> <d4 fis> <b g'> |
	<d16 fis> <a e'> <d fis> <fis, d'> <a8 e'> <a e'> <fis2 d'> |

	\bar "|.";
}

bass = \notes \relative c, \context Voice = bass {
	e2 r8 e16 e e8 e |
	g2 r8 g16 g g8 g |
	bes2 r8 bes16 bes bes8 bes |
	d4. d16 d g8 f e d |

	c4. c8 c4 f |
	c4. c8 g'4 g, |
	c4. c8 c4 f |
	g4 g, c8 g c4 |

	c4. c8 c4 f |
	c4. c8 g'4 g16 f e d |
	c4. c8 c4 f |
	g4 g, c8 g c4 |

	\property Staff.VoltaBracket = \turnOff
	\repeat volta 2 {
		f4. f8 e4 b' |
		f4. f8 a4 a, |
		d4. d8 a'4 a, |
	} \alternative {
		{ e'4. e8 a4 a, | }
		{ bes4. bes8 a'16 gis a e g e cis a | }
	}

	\key d\major;
	\bar "||";

	d4. d8 d4 g |
	d4. d8 a'4 a, |
	d4. d8 d4 g |
	a4 a, d8 a d4 |

	d4. d8 d4 g |
	d4. d8 a'4 a16 g fis e |
	d4. d8 d4 g |
	a4 a, d8 a d4 |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 110; }

	\paper {
		indent = 0;
		interscoreline = 6;
		\include "a4.ly"
	}
}
