\version "1.3.129";

\header {
	title = "Lord Blackthorn";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Markus Brenner";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key g\minor;
	\time 4/4;
}

melody = \notes \relative c' \context Voice = melody {
	\repeat volta 2 {
		<bes8. d g> <bes16 d g> <cis2 fis a> <bes8. d g> <bes16 d g> |
		<d8. g bes> <d16 g bes> <c2 es fis a> r4 |
		<bes8. d g> <bes16 d g> <cis2 fis a> \times 2/3 {g'8( bes )d} |
		<g,2 bes d g> r2 |
		\break

		<c8. es g> <c16 es g> <g2 c es> \times 2/3 {c8 d es} |
		<\context Voice = melodyTwo {\stemDown <g,4 bes>} {\stemUp d'8. bes16}> \stemBoth <bes,2 d g> r4 |
	} \alternative {
		{ <es4 gis c> <es gis bes> <d fis a> <g a c es> |
		  <fis2. a d> r4 | }
		{ <c4 es gis> <c es g> <c d fis> <c d fis a> |
		  <bes2 d g> r4 \times 2/3 {g'8 bes d} | }
	}
	\break

	<\context Voice = melodyTwo {\stemDown r4 <bes' es> <bes d>} {\stemUp g'2 f4}> \stemBoth \times 2/3 {f8 bes d8} |
	<\context Voice = melodyTwo {\stemDown r4 <a d> <a c>} {\stemUp f'2 e4}> \stemBoth \times 2/3 {e8 a c8} |
	<\context Voice = melodyTwo {\stemDown r4 <g c> <g bes>} {\stemUp es'2 d4}> <d4 g bes> |
	<d g a> <d fis c' d> <bes d g> \times 2/3 {g'8 bes d} |
	\break

	<\context Voice = melodyTwo {\stemDown r4 <bes es> <bes d>} {\stemUp g'2 f4}> \stemBoth \times 2/3 {f8 bes d8} |
	<\context Voice = melodyTwo {\stemDown r4 <a d> <a c>} {\stemUp f'2 e4}> \stemBoth \times 2/3 {e8 a c8} |
	<\context Voice = melodyTwo {\stemDown r4 <g c> r <gis c>} {\stemUp es'2 es}> |
	<\context Voice = melodyTwo {\stemDown r4 <gis cis> <b d>} {\stemUp f'2 g4}> \stemBoth r4 |
	\break

	\key c\minor;
	\bar "||";

	\repeat volta 2 {
		<es,8. g c> <es16 g c> <fis2 b d> <es8. g c> <es16 g c> |
		<g8. c es> <g16 c es> <f2 as b d> r4 |
		<es8. g c> <es16 g c> <fis2 b d> \times 2/3 {c'8( es )g} |
		<c,2 es g c> r2 |
		\break

		<f8. as c> <f16 as c> <c2 f as> \times 2/3 {f8 g as} |
		<\context Voice = melodyTwo {\stemDown <c,4 es>} {\stemUp g'8. es16}> \stemBoth <es,2 g c> r4 |
	} \alternative {
		{ <as4 cis f> <as cis es> <g b d> <c d f as> |
		  <b2. d g> r4 | }
		{ <f4 as cis> <f as c> <f g b> <f g b d> |
		  <es1 g c-\fermata> | }
	}

	\bar "|.";
}

bass = \notes \relative c \context Voice = bass {
	\property Staff.VoltaBracket = \turnOff
	\repeat volta 2 {
		g4 g g g |
		g g g d' |
		g, g g g |
		g g g'8. f16 es8. d16 |

		c4 c c c |
		g g g g |
	} \alternative {
		{ gis4 gis a a |
		  d d d8. c16 bes8. a16 | }
		{ gis4 gis a d |
		  g, g g \times 2/3 {g'8 bes d} | }
	}

	g''4 <es,, es'> <bes' bes'> \times 2/3 {f'8 bes d} |
	f4 <d,, d'> <a' a'> \times 2/3 {e'8 a c} |
	es4 <c,, c'> <g' g'> r4 |
	<d2 d'> <g4 g'> \times 2/3 {g'8 bes d} |

	g4 <es,, es'> <bes' bes'> \times 2/3 {f'8 bes d} |
	f4 <d,, d'> <a' a'> \times 2/3 {e'8 a c} |
	r4 <c,, c'> r <gis' gis'> |
	r <cis, cis'> <g' g'> r |

	\key c\minor;
	\bar "||";

	\repeat volta 2 {
		c4 c c c |
		c c c g' |
		c, c c c |
		c c c'8. bes16 as8. g16 |

		f4 f f f |
		c c c c |
	} \alternative {
		{ cis4 cis d d |
		  g g g8. f16 es8. d16 | }
		{ cis4 cis d g |
		  c, c c2-\fermata | }
	}

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 100; }

	\paper {
		indent = 0;
		interscoreline = 20;
		\include "a4.ly"
	}
}
