\version "1.3.129";

\header {
	title = "The Wisps";
	subtitle = "From Ultima VII --- The Black Gate";
	composer = "H.~Miller";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1992 Origin Systems, Inc.";
}

global = \notes {
	\key d\minor;
	\time 4/4;
}

melody = \notes \relative c'' {
	d2 a' |
	c4 b g2 |
	bes4 a as4. f8 |
	g4 e f4. e8 |

	d2 a' |
	c4 b g2 |
	f8 a g f e8. f32 e d8 c |
	e4 d d2 |

	< \context Voice = melodyOne { \stemUp
		d2 a' |
		c4 b g2 |
		bes4 a as4. f8 |
		g4 e f4. e8 |

		d2 a' |
		c4 b g2 |
		f8 a g f e8. f32 e d8 c |
		<a4 d e> <e a d> <f2 a d> |
	} \context Voice = melodyTwo { \stemDown
		<f2 a> r8 f' d a |
		<d2 a'> r4 d |
		<d2 f> <d4. e> r8 |
		<a2 des> <a4. des> r8 |

		<f2 a> r8 f' d a |
		<d2 a'> d |
		<a2 d> <a2 c> |
		\skip 1; |
	} > \stemBoth

	f'8_"rit." a g f e8. f32 e d8 c |
	e4 d d2 |

	\bar "|.";
}

bass = \notes \relative c, {
	d8( a' d e \stemUp )f4 \stemBoth d, |
	g8( d' g )a <d,2 g b> |
	d,8( bes' d )e <d4 as'> bes |
	a8( des e )g <e( bes'> a, <e' a> )a, |

	d,8( a' d e \stemUp )f4 \stemBoth d, |
	g8( d' g )a <d,4 bes'> g, |
	d8( a' d )a <e'( a> a, d )c |
	d,8( a' <c e> )a <a2 d f> |

	d,8( a' d e \stemUp )f4 \stemBoth d, |
	g8( d' g )a <d,2 g b> |
	d,8( bes' d )e <d8 as'> bes d4 |
	a8( des e )g <e( bes'> a, <e' a> )a, |

	d,8( a' d e \stemUp )f4 \stemBoth d, |
	g8( d' g )a <d,4 bes'> g, |
	d8( a' d )a <e'( a> a, d )c |
	d,8( a' <c e> )a <a2 d f> |

	bes2 a |
	d, d' |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 80; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
