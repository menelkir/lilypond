\version "1.3.129";

\header {
	title = "Wilderness";
	subtitle = "From Ultima VII --- The Black Gate";
	composer = "D.~Glover";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1992 Origin Systems, Inc.";
}

global = \notes {
	\key bes\minor;
	\time 3/4;
}

melody = \notes \relative c'' {
	\repeat unfold 2 {
		bes8 f c' f, des' f, |
		bes f c' f, des' f, |
		es' f, des' f, c' f, |
		bes f c' f, bes4 |
		\break
	}

	des16 bes' bes, f' r2 |
	ges16 des ges, b r2 |
	r4 as'8 es as, c |
	a16 c f f, r2 |

	r4 bes'8 bes, des f |
	r4 ges8 ges, bes des |
	a16 f' f, c' r2 |
	r4 des16 f, f' bes, r4 |

	\bar "|.";
}

bass = \notes \relative c' {
	bes8 f c' f, des' f, |
	bes f c' f, des' f, |
	es' f, des' f, c' f, |
	bes f c' f, bes4 |

	<bes,2 bes'> <des4 des'> |
	<c2 c'> <ges4 ges'> |
	<f2 f'> <as4 as'> |
	<bes2. bes'> |

	bes4 bes' f8 bes |
	ges,4 ges' des8 ges |
	as,4 as' es8 as |
	f,4 f' c8 f |

	bes,4 bes' f8 bes |
	ges,4 ges' des8 ges |
	f,4 f' c8 f |
	bes,4 bes' r |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 90; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
