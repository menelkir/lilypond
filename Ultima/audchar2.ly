\version "1.3.129";

\header {
	title = "Audchar Gargl Zenm�r (Song Of The Gargoyles)";
	subtitle = "From Ultima VI --- The False Prophet";
	composer = "Kenneth W. Arnold";
	instrument = "Piano Arrangement";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1990 Origin Systems, Inc.";
}

global = \notes {
	\key e\minor;
	\time 4/4;
}

melodyOne = \notes \relative c'' \context Voice = melodyOne { \stemUp \tieUp
	b2-\prall g4 a |
	b d8 c b4 c8 d |
	e4 c8 b a g fis4 |
	b8-\prall a g2 a4 |

	b2 g4. a8 |
	b4 c8 e d b c4 |
	b c8 b a b g4 |
	a8-\prall g fis2. ~ |

	fis8 g fis g a b g a |
	fis e fis g fis d e4 |
	fis8 g a4. g8 fis g |
	fis g a b a b c a |

	b4 g8 fis g4 b |
	c8 b a g e2 |
	a8 b a b c4 a |
	d8 c d4 a fis |

	g a8 b a b c4 |
	d e8 c b a b4 |
	g b8 d c b c4 |
	b8 c d c b a g a |

	g4 d8 g b a g4 |
	e8 fis g4 e4. d8 |
	e4 fis8 g a4 b |
	a fis8 g a4 fis |

	g a8 b a g fis4 |
	g a8 fis g fis g a |
	b a b c d4 c |
	b8 a b2. |

	\bar "|.";
}

melodyTwo = \notes \relative c'' \context Voice = melodyTwo { \stemDown
	g2 e4 fis |
	g4 b8 a g4 a8 b |
	c4 a8 g fis e dis4 |
	g8 fis e2 fis4 |

	g2 d4. fis8 |
	g4 a8 c b g a4 |
	g a8 g fis g e4 |
	fis4. d8 c d e c |

	d e d e fis g e fis |
	d c d e b4 cis |
	dis8 e fis4. e8 dis e |
	dis e fis g fis g a fis |

	g4 e8 dis e4 g |
	e8 d c b c2 |
	f8 g f g a4 f |
	a g8 a fis4 dis |

	e fis8 g fis g a4 |
	b c8 a g fis g4 |
	e g8 b a g a4 |
	fis8 e dis e fis4 c |

	b g8 b g' d b4 |
	g8 b e b g4 b |
	c dis8 e fis4 g |
	fis dis8 e fis 4 dis |

	e fis8 g fis e dis4 |
	e fis8 dis e dis e fis |
	g fis g a b4 a |
	g8 fis g fis e2 |

	\bar "|.";
}

bassOne = \notes \relative c \context Voice = bassOne { \stemUp
	e2 g4 e |
	g a b g |
	a2. b4 |
	e,2. fis4 |

	g a b2 |
	c4 e, d'2 |
	d g,4 c |
	a1 |

	fis4 g a g |
	fis2 gis4 ais |
	b2 ais |
	b4 a g fis |

	\stemBoth e2 b4 e | \stemUp
	g e g2 |
	f4 g a f |
	d'2 b |

	b2 c |
	g fis |
	r c' |
	b fis |

	g2 g4 d |
	e2. g4 |
	r2 a |
	b4 a g fis |

	e2 fis4 a |
	g2 a |
	b1 |
	\stemBoth e,4 fis g2 | \stemUp

	\bar "|.";
}

bassTwo = \notes \relative c \context Voice = bassTwo { \stemDown
	r2 c |
	d1 |
	fis2 b, |
	r b |

	e2 g |
	a fis |
	g r |
	d a |

	d4 e fis e |
	d2 cis |
	b fis' |
	dis1 |

	\skip 1; |
	c2 c |
	c1 |
	fis?2 dis4 b |

	e2 g4 e |
	d2 dis |
	e2. fis4 |
	dis2 b4 d |

	d2 b |
	e, b' |
	a'4 g fis e |
	e2 dis |

	r2 b |
	c e4 fis8 e |
	dis2 b |
	\skip 1; |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			<\melodyOne \melodyTwo>
		}
		\context Staff = bottom { \clef bass;
			\global
			<\bassOne \bassTwo>
		}
	>

%	\midi { \tempo 4 = 180; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
