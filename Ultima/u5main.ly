\version "1.3.129";

\header {
	title = "Main Theme";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Markus Brenner";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key f\minor;
	\time 4/4;
}

melody = \notes \relative c' \context Voice = melody {
	\repeat unfold 2 {
		f1 | r1 |
	}
	\break

	\repeat unfold 2 {
		<f'2 c'> ~ \times 2/3 { <f4 c'> <c as'> <as f'> } |
		<bes2 ges'> <des4 bes'> <ges des'> |
	}
	\break
	<f2 c'> ~ \times 2/3 { <f4 c'> <c' f> <f, c'> } |
	<bes2 es> ~ \times 2/3 { <bes4 es> <es, bes'> <bes' es> } |
	<des,2 as'> ~ \times 2/3 { <des4 as'> <as' des> <des, as'> } |
	<g2 c> r2 |
	\break

	\repeat unfold 2 {
		<f2 as c> ~ \times 2/3 { <f4 as c> <c f as> <as c f> } |
		<bes2 des ges> <des4 ges bes> <ges bes des> |
	}
	\break
	<f2 as c> ~ \times 2/3 { <f4 as c> <as c f> <f as c> } |
	<g2 bes es> ~ \times 2/3 { <g4 bes es> <es g bes> <g bes es> } |
	<f2 as des> ~ \times 2/3 { <f4 as des> <des f as c> <des f g bes> } |
	<c2 f g c> ~ <c e g c> |
	\break

	r4 f,8. f16 c'4. c16 d |
	es8 d c bes c4 es |
	r4 f8. f16 as4. bes16 c |
	g8 f c f g e c e |
	\break

	r4 f8. f16 c'4. c16 d |
	es8 d c bes f'2 |
	r4 c8. c16 f4 f8. f16 |
	c'1 ~ |
	c1 |
	\break

	\repeat volta 2 {
		<a,2 c f> ~ \times 2/3 { <a4 c f> <f a c> <c f a> } |
		<f2 as des> <es g bes> |
		<f2 a c> ~ \times 2/3 { <f4 a c> <c f a> <a c f> } |
		<as2 c es> <f bes d> |
	}
	\bar ":|";
	<e1 g c> |
	\break

	\key c\minor;
	\bar "||";

	\repeat unfold 2 {
		<c'2 g'> ~ \times 2/3 { <c4 g'> <g es'> <es c'> } |
		<f2 des'> <as4 f'> <des as'> |
	}
	\break
	<c2 g'> ~ \times 2/3 { <c4 g'> <g' c> <c, g'> } |
	<f2 bes> ~ \times 2/3 { <f4 bes> <bes, f'> <f'4 bes> } |
	<as,2 es'> ~ \times 2/3 { <as4 es'> <es' as> <as, es'> } |
	<d2 g> r2 |
	\break

	\repeat unfold 2 {
		<c2 es g> ~ \times 2/3 { <c4 es g> <g c es> <es g c> } |
		<f2 as des> <as4 des f> <des f as> |
	}
	\break
	<c2 es g> ~ \times 2/3 { <c4 es g> <es g c> <c es g> } |
	<d2 f bes> ~ \times 2/3 { <d4 f bes> <bes d f> <d f bes> } |
	<c2 es as> ~ \times 2/3 { <c4 es as> <as c es g> <as c d f> } |
	<g2 c d g> ~ <g2 b d g> |
	\break

	<
		\context Voice = melodyUpper { \stemUp
			g'2 a4. bes16 a |
			g4. es8 a2 |
			g2 a4. bes16 a |
			g4. es8 f2 |
		}
		\context Voice = melodyMiddle { \stemDown
			\repeat unfold 3 {
				<c2 es> <c f> |
			}
			<c2 es> <f, c'> |
		}
	>
	\break

	<
		\context Voice = melodyUpper { \stemUp
			as2 ges4. as16 bes |
			as4. es8 ges2 |
			as2 ges4. as16 bes |
			as4. bes8 c2 |
		}
		\context Voice = melodyMiddle { \stemDown
			\repeat unfold 3 {
				<c,2 es> <bes es> |
			}
			<c2 es> <c e> |
		}
	>

	\bar "|.";
}

bass = \notes \relative c, \context Voice = bass {
	\repeat unfold 2 {
		<f4. f'> <f8 f'> <f4 f'> <f4 f'> |
		<f4. f'> <f8 f'> <f2 f'> |
	}

	\repeat unfold 7 {
		<f4. f'> <f8 f'> <f4 f'> <f4 f'> |
	}
	<c'4. c'> <c8 c'> <c c'> <bes bes'> <as as'> <g g'> |

	\repeat unfold 7 {
		<f4. f'> <f8 f'> <f4 f'> <f4 f'> |
	}
	<c'4. c'4.> c'16 b c8 d e c |

	\clef treble;
	<as2 c f> as8( c f )as |
	<bes,2 es g bes> bes8( es g )bes |
	<des,2 f as c> des8( f as )des |
	<c,2 f g c> <c e g c> |

	<c2 f a c> c8( f a )c |
	<es,2 as c es> es8( as c )es |
	<f,2 bes d f> f8( bes d )f |
	<c2 f g c> ~ <c e g c> ~ |
	<c d g c> ~ <c e g c> |

	\clef bass;

	\repeat volta 2 {
		<f,,4. f'> <f8 f'> <f4 f'> <f f'> |
		<des4. des'> <des8 des'> <es4 es'> <es es'> |
		<f4. f'> <f8 f'> <f4 f'> <f f'> |
		<as4. es'> <as8 es'> <bes4 d> <bes d> |
	}
	\bar ":|";
	<c,4. c'> <c8 c'> <c2 c'> |

	\key c\minor;
	\bar "||";

	\repeat unfold 7 {
		<c,4. c'> <c8 c'> <c4 c'> <c4 c'> |
	}
	<g'4. g'> <g8 g'> <g g'> <f f'> <es es'> <d d'> |

	\repeat unfold 7 {
		<c4. c'> <c8 c'> <c4 c'> <c4 c'> |
	}
	<g'4. g'4.> g'16 fis g8 a b g |

	\repeat unfold 2 {
		c,,8 g' c c, f4 c'8 f, |
		c8 g' c c, f2 |
	}

	as,8 es' as as, es'4 bes'8 es, |
	as,8 es' as as, es'2 |
	as,8 es' as as, es'4 bes'8 es, |
	as,8 es' as as, c g' c c, |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 130; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
