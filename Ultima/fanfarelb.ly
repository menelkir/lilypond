\version "1.3.129";

\header {
	title = "Fanfare Of Lord British";
	subtitle = "From Ultima IV --- Quest Of The Avatar";
	composer = "Kenneth W. Arnold";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1985 Origin Systems, Inc.";
}

global = \notes {
	\key g\major;
	\time 3/4;
}

melody = \notes \relative c' \context Voice = melody {
	\repeat volta 2 {
		\repeat unfold 2 {
			<b8 g'> <d a'> <g4 b> \times 2/3 {<d8 a'> <g b> <d a'>} |
		}
		<b8 g'> <d a'> <g4 b> \times 2/3 {<a8 c> <g b> <a c>} |
		<fis4 a> <fis2 a> |
		\break

		<g8 b> <e g> <f4. a> <e16 g> <d f> |
		<e8 g> <c e> <dis2 fis> |
		<d8 g> <a d> <b4 d> <g d'> |
		<a d> <a2 d> |
	}
	\bar ":|";
	\break

	\repeat unfold 2 {
		<b8 e> <g b> <\context Voice = melodyTwo {\stemDown \property Voice.tupletInvisible = ##t a4 \times 2/3 {g8 a fis}} {\stemUp dis'4 ~ \times 2/3 {dis4 b8}}> | \stemBoth
	}
	<g8 e'> <a fis'> <b4 g'> \times 2/3 {<cis8 a'> <e g> <cis a'>} |
	<dis4 b'> <dis2 b'> |
	\break

	<a'8 c> <g b> <fis4. a> <e16 g> <fis a> |
	<g8 b> <fis a> <\context Voice = melodyTwo {\stemDown e2} {\stemUp g4. fis16 g}> | \stemBoth
	<c8 a'> <e g> <\context Voice = melodyTwo {\stemDown d2} {\stemUp fis4. e16 fis}> | \stemBoth
	<d8 g> <g b> <e c'> <d b'> <e c'> <c a'> |

	\bar ":|";
}

bass = \notes \relative c \context Voice = bass {
	\repeat volta 2 {
		g8 d' g4 d |
		g,8 d' g4 d |
		g,8 d' g4 g |
		d d4. dis8 |

		e8 b' c4 f, |
		c8 g' b4 a |
		g8 d g,4 g |
		d' d,2 |
	}
	\bar ":|";

	\property Voice.tupletDirection = \down
	e8 b' c4 \times 2/3 {<b8> <c> <a>} |
	e8 b' c4 \times 2/3 {<b8> <c> <a>} |
	e8 d g4 a |
	fis b,2 |

	a'8 b16 c d4 d, |
	g16 a b d c4 b |
	a16 b c e d4 c |
	b8 e d4 d,_"F.Q." |

	\bar ":|";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 100; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
