\version "1.3.129";

\header {
	title = "Stones";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "D.~Watson, K.~Jones";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key g\minor;
	\time 3/4;
	\partial 4;
}

melody = \notes \relative c'' {
	[g8 a] |

	\repeat volta 2 {
		bes4 bes bes |
		bes c bes |
		a a a |
		a bes a |
		\break
	} \alternative {
		{ \time 5/4;
		  g4_"rit." g-\prall g-\prall g8-\prall f d c |
		  \time 3/4;
		  d2 g8_"a tempo" a | }
		{ \time 5/4;
		  g4_"rit." g-\prall g-\prall g8-\prall f d f |
		  \time 3/4;
		  g2 g8_"a tempo" a | }
	}
	\break
	\time 4/4;

	\repeat volta 2 {
		bes2. c8 bes |
		a2. bes8 a |
		g2 ~ g8 a g f |
		d2. g8 a |

		bes2 a4 bes |
		c2 bes4 c |
		d1 ~ |
		d2 g,4 a |
		\break

		bes2 c4 bes |
		a2 bes4 a |
		g4. a8 g4 f |
		d2 g4 a |

		bes2 c4 bes |
		a2 g4. f8 |
		a4 g2. ~ |
	} \alternative {
		{ g2. g8 a | }
		{ g2. bes8 c | }
	}
	\break

	d'8 c2 bes8 c d ~ |
	d2 c4 bes |
	c8 bes4. a4 bes8 c ~ |
	c2 bes4 a |

	bes2 g4 a8 bes ~ |
	bes2 ~ bes8 a4 g8 |
	f1 ~ |
	f2. g8 a |
	\break

	bes8 a4. g4 bes8 a ~ |
	a2. bes8 a |
	bes4. a8 g f4 d8 ~ |
	d2 g4 a |

	bes2 c4. bes8 |
	a2 g4. f8 |
	a8 g8 ~ g2. ~ |
	g2. g8 a |
	\break

	bes2. c8 bes |
	a2. bes8 a |
	bes2 ~ bes8 a g bes |
	d,2. g8 a |

	bes2 a4 bes |
	c2 bes4 c |
	d1 ~ |
	d2 g,4 a |
	\break

	bes2 c4 bes |
	a2 bes4 a |
	g4. a8 g4 f |
	d2 g4 a |

	bes2 c4 bes |
	a2 g4. f8 |
	a4 g2. ~ |
	g1 |

	\bar "|.";
}

bassOne = \notes \relative c \context Voice = bassOne { \stemDown \tieDown
	\property Staff.VoltaBracket = \turnOff
	r4 |

	\repeat volta 2 {
		g2. ~ | g | f' ~ | f |
	} \alternative {
		{ \time 5/4; g,1 ~ g4 |
		  \time 3/4; d2. | }
		{ \time 5/4; g,1 ~ g4 |
		  \time 3/4; g2. | }
	}
	\time 4/4;

	\repeat volta 2 {
		g,1 | f | g | d |
		g | f | g | g |
		g | f | g | d |
		g | f | g |
	} \alternative {
		{ g | }
		{ g | }
	}

	bes1 | bes | f | f |
	g | g | d | d |
	g | f | g | d |
	g | f | g | g |

	g1 | f | g | d |
	g | f | g | g |
	g | f | g | d |
	g | f | g | g |

	\bar "|.";
}

bassTwo = \notes \relative c' \context Voice = bassTwo { \stemUp \tieUp
	\skip 4; |

	\repeat volta 2 {
		r8 d4 d d8 ~ |
		d8 d4 d d8 ~ |
		d8 d4 d d8 ~ |
		d8 d4 d d8 |
	} \alternative {
		{ \time 5/4; r1 r4 |
		  \time 3/4; r2. | }
		{ \time 5/4; r1 r4 |
		  \time 3/4; g,2. | }
	}
	\time 4/4;

	\repeat volta 2 {
		r8 d, g bes d, g bes d, |
		r c f a c, f a c, |
		r d g bes d, g bes d, |
		r a d a' a, d a' a, |

		r d g bes d, g bes d, |
		r c f a c, f a c, |
		r d g bes d, g bes d, |
		r d g bes d, g bes d, |

		r d g bes d, g bes d, |
		r c f a c, f a c, |
		r d g bes d, g bes d, |
		r a d a' a, d a' a, |

		r d g bes d, g bes d, |
		r c f a c, f a c, |
		r d g bes d, g bes d, |
	} \alternative {
		{ bes'1 | }
		{ bes'1 | }
	}

	r8 f bes d f, bes d f, |
	r f bes d f, bes d f, |
	r c f a c, f a c, |
	r c f a c, f a c, |

	r d g bes d, g bes d, |
	r d g bes d, g bes d, |
	r a d a' a, d a' a, |
	r a d a' a, d a' a, |

	r d g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	r a d a' a, d a' a, |

	r d g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	bes'1 |

	r8 d, g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	r a d a' a, d a' a, |

	r d g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	r d g bes d, g bes d, |

	r d g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	r a d a' a, d a' a, |

	r d g bes d, g bes d, |
	r c f a c, f a c, |
	r d g bes d, g bes d, |
	bes'1 |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			<\bassOne \bassTwo>
		}
	>

%	\midi { \tempo 4 = 120; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
