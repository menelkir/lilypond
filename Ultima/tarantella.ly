\version "1.3.129";

\header {
	title = "Villager Tarantella";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Markus Brenner";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key c\major;
	\time 3/4;
}

melody = \notes \relative c' \context Voice = melody {
	\repeat volta 2 {
		<e4. g c> <e8 g c> <e4 g c> |
		<f2 a c> <e4 g c> |
		<f2 bes d> <e4 g c> |
		<d2. g b> |

		<e4. g c> g8 <e4 g bes> |
		<c4 f bes> <c2 f a> |
		<c4 d g> <b2 d g> |
		<c2. e g> |
		\break

		<e4. g c> <e8 g c> <e4 g c> |
		<f2 a c> <e4 g c> |
		<f2 bes d> <e4 g c> |
		<d2. g b> |
	} \alternative {
		<
			\context Voice = melodyTwo { \stemDown
				f2. | f | f | f |
				es2. | es | es | f |
			}
			{ \stemUp
				d'4. c8 bes4 |
				a c f |
				d4. c8 bes4 |
				a2. |
				\break

				c4. bes8 as4 |
				g bes es |
				c4. bes8 as4 |
				d4. c8 b4 |
			}
		>
		{ \stemBoth
			<e4. g c> g8 <e4 g bes> |
			<c4 f bes> <c2 f a> |
			<d4 e a> <cis2 e a> |
			<d2. fis a> |
		}
	}

	\key g\minor;
	\bar "||";
	\break

	bes'4. c8 d4 |
	c2 a4 |
	d4. c8 bes4 |
	c2. |

	bes4. c8 d4 |
	bes4 a2 |
	g2. |
	g2. |
	\break

	bes4. c8 d4 |
	f4 es2 |
	d4. c8 bes4 |
	c2. |

	bes4. c8 d4 |
	bes4 a2 |
	g4. a8 f4 |
	g2. |

	\bar "|.";
}

bass = \notes \relative c' \context Voice = bass {
	\property Staff.VoltaBracket = \turnOff
	\repeat volta 2 {
		c4. c8 c4 |
		f,2 c'4 |
		bes2 c4 |
		g2. |

		c2 c4 |
		f,2. |
		g4 g2 |
		c,2. |

		c'4. c8 c4 |
		f,2 c'4 |
		bes2 c4 |
		g2. |
	} \alternative {
		{
			bes2. | f | bes | f |
			as | es | as | g |
		}
		{
			c2 c4 |
			f,2. |
			a4 a2 |
			d,2. |
		}
	}

	\key g\minor;
	\bar "||";

	<g,2. d'> |
	<f c'> |
	<bes f'> |
	<as es'> |

	<g d'> |
	<d d'> |
	<g2 d'> <g4 d'> |
	<g2. d'> |

	<g d'> |
	<a f'> |
	<bes f'> |
	<as es'> |

	<g d'> |
	<d d'> |
	<g2 d'> <f4 c'> |
	<g2. d'> |

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 200; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
