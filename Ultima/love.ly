\version "1.3.129";

\header {
	title = "Love Theme";
	subtitle = "From Ultima VII --- The Black Gate";
	composer = "R.~Benson";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1992 Origin Systems, Inc.";
}

global = \notes {
	\key a\minor;
	\time 3/4;
}

melody = \notes \relative c'' {
	c4 d e |
	g2 f8 e |
	a,4 b c |
	d2. |

	g,4 a b |
	c2. |
	b2 ~ b8 a16 b |
	a2. |
	\break

	e'2 f8 g |
	g4 d2 |
	c2 d8 e |
	e4 b2 |

	a2 b8 c |
	g2 e'8 c |
	d2 ~ d8 a |
	b2. |
	\break

	<c2 e> f8 g |
	<b,4 g'> <g2 d'> |
	<a2 c> d8 e |
	<g,4 e'> <e2 b'> |

	<f2 a> b8 c |
	<e,2 g> c'4 |
	<as2 b> ~ <as8 b> a16 b |
	<e,2. a> |
	\break

	<a4 c> <b d> <c e> |
	<b2-\arpeggio d g> f'8 e |
	<\context Voice = melodyTwo { \stemDown f,2. } { \stemUp a4 b c} > \stemBoth
	<g2.-\arpeggio b d> |

	<\context Voice = melodyTwo { \stemDown e2. } { \stemUp g4 a b} > \stemBoth
	<f2. a c> |
	<d2_"rit." g b> <d4 d'> |
	<e2. e'-\fermata> |
	\bar "|.";
}

accomp = \notes \relative c' {
	a4( \stemUp )e'2 \stemBoth |
	e,4( )b'2 |
	<f2. c'> |
	g4( b )d |

	e,2. |
	f4( c' )a |
	g4( d' )b |
	a8( c e4 )c |

	c8( e g e g )d |
	g,( b d4 )g |
	a,8( c e c e )b |
	e,( b' e4 )g, |

	f8( a c a c )g |
	c,( g' <)c4. e> a8 |
	d,4( d' )a |
	g8( b d4 )b |

	c,8( g' c e, g )c |
	g( d' g b, d )g |
	a,( c e c b )a |
	e( b' e g, b )e |

	f,8( a c a4 )c8 |
	c,8( g' c e, )c'4 |
	e,8( b' e4 )as, |
	a,8( c c'4 )e, |

	a,4. e'4 c8 |
	e,4 e' b8 c |
	f,4. f'4 c8 |
	g4 g' b, |

	e,2 e'4 |
	c4 f a, |
	g g' d |
	<a2.-\fermata a'> |

	\bar "|.";
}

bass = \notes \relative c {
	\repeat unfold 8 { r2. | }

	c2. | g | a | e' | f | c | d | g, |
	c | g | a | e' | f | c | e | a, |
	a | e' | f | g, | e' | f | g, | a-\fermata |

	\bar "|.";
}

\score {
	\context Score <
		\context PianoStaff <
			\context Staff = top {
				\global
				\melody
			}
			\context Staff = bottom { \clef bass;
				\global
				\accomp
			}
		>
		\context Staff = bass { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 100; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
