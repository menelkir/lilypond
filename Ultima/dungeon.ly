\version "1.3.129";

\header {
	title = "Dungeon Theme";
	subtitle = "From Ultima VI --- The False Prophet";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1990 Origin Systems, Inc.";
}

global = \notes {
	\key c\major;
	\time 4/4;
	\partial 8;
}

melody = \notes \relative c''' \context Voice = melody {
	r8 |

	\repeat unfold 6 {
		r1 |
	}
	\break

	d4. c8 ~ c2 |
	a4. g8 ~ g2 |

	<a,2 d f a> <g c e g> |
	<e a c e> <f4 a c f> <e a c e> |
	<a2 d f a> <g c e g> |
	<f a c f> <e a c e> |
	<a,1 d f a> ~ |
	<a d f a> |
	\break

	<a2 d f a> <c e g c> |
	<g' c e g> <f g d' e> |
	<a,2 d f a> <c e g c> |
	<f a c f> <e g c e> |
	<a,1 d f a> ~ |
	<a2 d f a> ~ <a4. d f a-\fermata>

	\bar "|.";
}

bass = \notes \relative c, \context Voice = bass {
	d8 |

	\repeat unfold 3 {
		d4. d8 d4. d8 |
		d4. d8 d4. d8 |

		d8 a' g a c4 g8 a |
		d, a' g a f4 e8 d |
		d a' g a c4 g8 a |
		f a f a e a e a |
	}

	d,4. d8 d4. d8 |
	d4. d8 d4.-\fermata

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 100; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
