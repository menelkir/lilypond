\version "1.3.129";

\header {
	title = "The Dark Path";
	subtitle = "From Ultima VII Part 2 --- Serpent Isle";
	composer = "D.~Glover";
	arranger = "Arranged by Christian Bauer";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1993 Origin Systems, Inc.";
}

global = \notes {
	\key e\minor;
	\time 4/4;
}

melody = \notes \relative c' {
	r1 |
	r2. <e16-\staccato bes' e> <e-\staccato bes' e> r8 |

	\repeat volta 2 {
		g8 b e, b' g b e, b' |
		g bes e, bes' g bes e, bes' |
	}
	\bar ":|";
	\break

	r4 e, b'2 |
	g16 fis e4 bes'4. a8 g |
	r4 e' b'2 |
	g16 fis e4 bes'4. a8 g |
	\break

	r8 e,-. c'-\accent e,4-. e-. e8-. |
	bes'-\accent e,4-. e-. e-. e8-. |
	g8 b e, b' g b e, b' |
	g bes e, bes' g bes e, bes' |
	\break

	\key bes\minor;

	r1 \> | r | r | \! r |

	\bar "|.";
}

bassRiff = \notes {
	g8-\accent e4-. e-. e-. e8-. |
	bes'-\accent e,4-. e-. e8-. c'-\accent e,-. |
}

bass = \notes \relative c, {
	<e1 e'> ~ |
	<e e'> |

	\repeat volta 2 { \bassRiff }
	\repeat unfold 2 { \bassRiff }

	r8 e-. c'-\accent e,4-. e-. e8-. |
	bes'-\accent e,4-. e-. e-. e8-. |
	\bassRiff

	\key bes\minor;

	\repeat unfold 2 {
		des8-\accent bes4-. bes-. bes-. bes8-. |
		e-\accent bes4-. bes-. bes8-. ges'-\accent bes,-. |
	}

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 110; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
