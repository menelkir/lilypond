\version "1.3.129";

\header {
	title = "Forest Theme";
	subtitle = "From Ultima VI --- The False Prophet";
	composer = "Kenneth W. Arnold";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1990 Origin Systems, Inc.";
}

global = \notes {
	\key a\minor;
	\time 4/4;
	\partial 4;
}

melody = \notes \relative c'' {
	c8 b |

	a g a b c4 c8 b |
	a g a b c4 c8 b |
	a g a b c b a gis |
	a2. c8 b |
	\break

	\repeat volta 2 {
		a-\segno g a b c d e f |
		g f e d c4 c8 b |
		a g a b c d e f |
		g f e d e4 c8 b |
		\break

		a g a b c d e f |
		g f e d c4 c8 b |
		a g a g gis b e d |
	} \alternative {
		{ c b a g a4^"Fine" c8 b | }
		{ c b a g a4 r | }
	}
	\break

	\repeat volta 2 {
		f8 e d4 g8 f e d |
		e d c b c d e g |
		f8 e d4 e8 d c b |
		c b a g a4 e'8 g |
		\break

		f8 e d4 g8 f e d |
		e d e d c e d c |
		e d c b c b a gis |
	} \alternative {
		{ a1 | }
		{ a2. c8 b | }
	}

	\bar "|.";
}

bassOne = \notes \relative c \context Voice = bassOne { \stemDown
	\property Staff.VoltaBracket = \turnOff
	r4 |

	a2 f4 g |
	a2 f4 g |
	a2 f4 e |
	a2 e |

	\repeat volta 2 {
		a2 f |
		g r |
		a f |
		g r |

		a f |
		g r |
		a e |
	} \alternative {
		{ f4 e2. | }
		{ f4 e2 r4 | }
	}

	\repeat volta 2 {
		d'2 g,4 b |
		c2 c |
		d e,4 gis |
		a2 a |

		d g,4 b|
		c4 b a2 |
		f e |
	} \alternative {
		{ a2. r4 | }
		{ a2. r4_"D.S."_"al Fine" | }
	}

	\bar "|.";
}

bassTwo = \notes \relative c \context Voice = bassTwo { \stemUp \tieUp
	\skip 4; |

	r4 e r2 |
	r4 e r2 |
	r4 e r2 |
	r4 e2. |

	\repeat volta 2 {
		r4 e2 f4 ~ |
		f4 d c b |
		r e2 f4 ~ |
		f4 d c b |

		r e2 f4 ~ |
		f4 d c b |
		r e2 e4 ~ |
	} \alternative {
		{ e2 a, | }
		{ e2 a,4 \skip 4; | }
	}

	\repeat volta 2 {
		r4 a8 g ~ g2 |
		r4 e2 e4 |
		r4 a8 g ~ g2 |
		r4 e2. |

		r4 a8 g ~ g2 |
		r2. e4 ~ |
		e4 f2 e4 ~ |
	} \alternative {
		{ e2. \skip 4; | }
		{ e2. \skip 4; | }
	}

	\bar "|.";
}

\score {
	\context PianoStaff \notes <
		\context Staff = top {
			\global
			\melody
		}
		\context Staff = bottom { \clef bass;
			\global
			<\bassOne \bassTwo>
		}
	>

%	\midi { \tempo 4 = 130; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
