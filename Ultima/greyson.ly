\version "1.3.129";

\header {
	title = "Greyson's Tale";
	subtitle = "From Ultima V --- Warriors Of Destiny";
	composer = "Kenneth W. Arnold";
	arranger = "Arranged by Markus Brenner";
	tagline = "Typeset by Christian Bauer $<$Christian.Bauer@uni-mainz.de$>$";
	enteredby = "Christian Bauer";
	copyright = "1988 Origin Systems, Inc.";
}

global = \notes {
	\key a\minor;
	\time 3/4;
	\partial 4;
}

melody = \notes \relative c' \context Voice = melody {
	[c8 d] |

	e4 e e |
	d2 c8 d |
	e4 g g |
	g2 f4 |
	e4. d8 c4 |
	b2 e4 |
	c2. |
	a2 c8 d |
	\break

	e4 e e |
	d2 e8 f |
	g4 g g |
	f2 e8 d |
	c4 e e |
	d2 c8 b |
	a4 a a |
	a2^"Fine" a'8 b |
	\break

	< \context Voice = melodyTwo {\stemDown f2.} {\stemUp c'4. b8 a4} > | \stemBoth
	g4 c g |
	a4. g8 f4 |
	e4 c b'8 c |
	< \context Voice = melodyTwo {\stemDown f,2.} {\stemUp d'4. c8 b4} > |
	< \context Voice = melodyTwo {g2.} {b4. c8 d4} > | \stemBoth
	c2. |
	b2 a8 b |
	\break

	c4. b8 a4 |
	a4. b8 c4 |
	b4. a8 g4 |
	a2 a8 b |
	c4. d8 e4 |
	g2 f4 |
	e2. ~ |
	e2 a,,8 b |
	\break

	<a4 c> <a c> <a c> |
	<f'4. a> <e8 g> <d4 f> |
	<c4 e> <a c> <a c> |
	< \context Voice = melodyTwo {\stemDown gis2 gis4} {b4. a8 b4} > |
	<a4 c> <c e> <e a> |
	<a4. c> <g8 b> <f4 a> |
	< \context Voice = melodyTwo {\stemDown b2 a4} {\stemUp d4. c8 d4} > |
	< \context Voice = melodyTwo {\stemDown gis2.} {b2 c8 b} > | \stemBoth
	\break

	<a8 c> <g b> <e a> <g b> <a4 c> |
	<g8 b> <fis a> <e g> <fis a> <g4 b> |
	<f8 a> <c g'> <a f'> <c g'> <f4 a> |
	<e4 g> <c2 e> |
	<d8 f> <a e'> <f d'> <a e'> <d4 f> |
	<c8 e> <b d> <a c> <b d> <c4 e> |
	<d8 f> <c e> <b d> <d f> <f4 a> |
	<e2 gis>
	\break

	\bar "|.";
}

accomp = \notes \relative c \context Voice = guitar {
	r4 |

	<e2. a c> |
	<d g b> |
	<g c e> |
	<d b'> |
	<e a c> |
	<b e b'> |
	<e a c> |
	<a, e' a> |

	<e' a c> |
	<d g b> |
	<g c e> |
	<c, f a> |
	<e a c> |
	<b e gis> |
	<c4 e a> <c e a> <c e a> |
	<c2 e a> r4 |

	<c2. a'> |
	<g' c e> |
	<c, f c'> |
	<e g c> |
	<d a'> |
	<d b'> |
	r2 <e4 a c> |
	<e2. gis b> |

	<e a c> |
	<c f c'> |
	<d g b> |
	<c e a> | \clef treble;
	<e' a> |
	<f a> |
	r4 <e4 gis b> <e gis b> |
	<e2 gis b> r4 | \clef bass;

	<e,2. a> |
	<d a'> |
	<e a> |
	<e gis> |
	<e a> |
	<c a'> |
	<b e a> |
	<b e gis> |

	<e a> |
	<b e> |
	<c f> |
	<g e'> |
	<a d> |
	<a> |
	<b f' a> |
	<gis'2 b e>

	\bar "|.";
}

bass = \notes \relative c \context Voice = bass {
	r4 |

	a2. | g | c | g | a | e | a2 e4 | a2. |
	a | g | c | f, | a | e | a4 a a | a2 r4 |
	f2. | c' | f, | c' | d, | g | a | e |
	a | f | g | a | e' | d | e ~ | e |
	a, | d | a | e | a | f | e | e |
	a | e | f | c | d | e | d | e2_"D.C."_"al Fine"

	\bar "|.";
}

names = \chords {
	r4
	a2.:min g c g a:min e a:min a:min
	a:min g c f a:min e a:min a:min
	f c f c d:min g r2 a4:min e2.
	a:min f g a:min a:min d:min r4 e2 e2.
	a:min d:min a:min e a:min f \notes{<e a b>} e a:min
	e:min f c d:min a:min d:6 e
}

\score {
	\context Score <
		\context PianoStaff <
			\context Staff = top {
				\global
				\melody
			}
			\context Staff = bottom { \clef bass;
				\global
				\accomp
			}
		>
		\context ChordNames \names
		\context Staff = bass { \clef bass;
			\global
			\bass
		}
	>

%	\midi { \tempo 4 = 160; }

	\paper {
		indent = 0;
		\include "a4.ly"
	}
}
